package scripts.tutorialislandscript.gui;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import scripts.modules.fluffeesapi.data.accounts.AccountDetails;
import scripts.modules.fluffeesapi.data.network.ConnectionSettings;
import scripts.modules.fluffeesapi.scripting.antiban.AntiBanSingleton;
import scripts.modules.fluffeesapi.scripting.swingcomponents.events.CloseGuiEvent;
import scripts.modules.fluffeesapi.scripting.swingcomponents.gui.wizard.AbstractWizardGui;
import scripts.modules.fluffeesapi.scripting.swingcomponents.gui.wizard.AbstractWizardStepPanel;
import scripts.modules.fluffeesapi.scripting.swingcomponents.inputs.*;
import scripts.modules.fluffeesapi.scripting.swingcomponents.inputs.buttons.FxButton;
import scripts.modules.fluffeesapi.scripting.swingcomponents.inputs.buttons.WizardStartButton;
import scripts.modules.fluffeesapi.scripting.swingcomponents.jiconfont.icons.FontAwesome;
import scripts.modules.fluffeesapi.scripting.swingcomponents.panels.InputPanel;
import scripts.modules.fluffeesapi.scripting.swingcomponents.panels.stepcontents.ColumnPanel;
import scripts.modules.fluffeesapi.scripting.swingcomponents.panels.stepcontents.InputColumn;
import scripts.modules.fluffeesapi.scripting.swingcomponents.panels.stepcontents.ListColumn;
import scripts.modules.fluffeesapi.scripting.swingcomponents.panels.wizard.StartButtonPane;
import scripts.modules.fluffeesapi.utilities.Utilities;
import scripts.modules.fluffeesapi.web.accountCreation.captcha.CaptchaSolver;
import scripts.tutorialislandscript.data.RSLocation;
import scripts.tutorialislandscript.data.Variables;
import scripts.tutorialislandscript.gui.paths.CreateAccountsPath;
import scripts.tutorialislandscript.gui.paths.LoadAccountsPath;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.*;

public class TutorialIslandGui extends AbstractWizardGui {

    private Variables.VariablesBuilder variablesBuilder;

    private StartButtonPane pnlWizardStart;
    public static final Color ACCENT_COLOR = new Color(99, 130, 191);

    public TutorialIslandGui(Variables.VariablesBuilder variablesBuilder) {
        super("Fluffee's Tutorial Island", "Creates accounts and completes tutorial island");

        this.variablesBuilder = variablesBuilder;

//        this.setHelpFrame(new HelpFrame("Help"));
//        this.getHelpframe().setHtmlText("<h1 style=\"padding-top: 0px;\" id=\"accountcreation\">" +
//                "Account Creation</h1>\n" +
//                "\n" +
//                "<h2 id=\"generatingvalues\">Generating Values</h2>\n" +
//                "\n" +
//                "<p>The script supports generating words and numbers in the account's username, email or password. " +
//                "To generate a word you'll need the length of the word you'd like to generate. As an example, if you " +
//                "wanted to generate a 5 letter word in your email you would type \"[S5]\" in the email field " +
//                "(without quotes). To generate a number you'll need to the first number you want to start generating " +
//                "from (the number increases by 1 with every account created). For example, if you wanted to " +
//                "generate a number starting from 3 in your email you would type \"[N3]\" in the email field " +
//                "(without quotes).</p>\n" +
//                "\n" +
//                "<p>You can also generate multiple words or numbers at a time. As an example you could type " +
//                "something like: \"[S5][S4][N11]@gmail.com\" which could generate something like: " +
//                "\"HelloJohn11@gmail.com\".</p>\n" +
//                "\n" +
//                "<h2 id=\"emailaddresses\">Email Addresses</h2>\n" +
//                "\n" +
//                "<p>The email address you enter does not have to be a valid email, the account will create " +
//                "regardless. However, if you don't use a valid email you won't be able to verify the email " +
//                "and potentially unlock the account if necessary.</p>\n" +
//                "\n" +
//                "<h2 id=\"username\">Username</h2>\n" +
//                "\n" +
//                "<p>If you use username generation (see Generating Values) if the username is taken, the " +
//                "script will generate a new username and try to claim it. Otherwise the account will logout, " +
//                "and we'll move on to the next.</p>\n" +
//                "\n" +
//                "<h2 id=\"captchaprovider\">Captcha Provider</h2>\n" +
//                "\n" +
//                "<p>The captcha provider is the service you're using to solve your captchas, as each account " +
//                "created requires a captcha. This will require you to load an account on said site with credits, " +
//                "they're sadly not free.</p>\n" +
//                "\n" +
//                "<h1 id=\"globalsettings\">Global Settings</h1>\n" +
//                "\n" +
//                "<h2 id=\"leavelastaccountloggedin\">Leave Last Account Logged In</h2>\n" +
//                "\n" +
//                "<p>This setting will keep the last account in the list logged in before ending the script, " +
//                "instead of logging it out.</p>\n" +
//                "\n" +
//                "<h2 id=\"endinglocation\">Ending Location</h2>\n" +
//                "\n" +
//                "<p>This dictates where you want the accounts to walk after they complete tutorial island. " +
//                "That way all of your accounts can be at the GE when you want to use them.</p>\n" +
//                "\n" +
//                "<h2 id=\"endingitems\">Ending Items</h2>\n" +
//                "\n" +
//                "<p>This dictates what you want the script to do with the account's items. These are the items " +
//                "that are placed in the account's inventory after completing tutorial island. The items can be " +
//                "banked, dropped or just left in the inventory.</p>\n" +
//                "\n" +
//                "<h2 id=\"abc2sleepmodifier\">ABC2 Sleep Modifier</h2>\n" +
//                "\n" +
//                "<p>This dictates the modifier you want to apply to all ABC2 (antiban) sleeps. The lower the " +
//                "setting the faster the script will run, as it will sleep for less time.</p>\n" +
//                "\n" +
//                "<h2 id=\"chatsleepmodifier\">Chat sleep modifier</h2>\n" +
//                "\n" +
//                "<p>This dictates the modifer you want to apply to all chat sleeps (these are the sleeps between " +
//                "clicking the click continue option). The lower the setting the faster the script will run, as it " +
//                "will sleep for less time.</p>\n" +
//                "\n" +
//                "<h1 id=\"networksettings\">Network Settings</h1>\n" +
//                "\n" +
//                "<h2 id=\"proxyip\">Proxy IP</h2>\n" +
//                "\n" +
//                "<p>This is the IP of the proxy you want to use for account creation. TRiBot may already be " +
//                "proxifying the request, but it doesn't hurt to be extra careful.</p>\n" +
//                "\n" +
//                "<h2 id=\"proxyport\">Proxy Port</h2>\n" +
//                "\n" +
//                "<p>This is the port of the proxy you want to use for account creation.</p>\n" +
//                "\n" +
//                "<h2 id=\"proxyusername\">Proxy Username</h2>\n" +
//                "\n" +
//                "<p>This is the username of the proxy you want to use for account creation. If your proxy does " +
//                "not have a username and password for authentication please leave this field blank.\"</p>\n" +
//                "\n" +
//                "<h2 id=\"proxypassword\">Proxy Password</h2>\n" +
//                "\n" +
//                "<p>This is the password of the proxy that you want to use for account creation.</p>");

        setupStartButtonPane();
    }

    @Override
    protected void setupStartButtonPane() {
        pnlWizardStart = new StartButtonPane(StartButtonPane.ButtonLayout.THREE_BUTTONS);
        WizardStartButton btnCreateAccounts = new WizardStartButton("Create Accounts",
                ACCENT_COLOR,
                StartButtonPane.ButtonLayout.THREE_BUTTONS,
                FontAwesome.USERS);
        btnCreateAccounts.addActionListener(e -> {
            btnCreateAccounts.setEnabled(false);
            setGuiPath(new CreateAccountsPath(variablesBuilder));
            slider.slideLeft();
        });
        pnlWizardStart.addButton(btnCreateAccounts);

        WizardStartButton btnLoadAccounts = new WizardStartButton("Load Accounts",
                ACCENT_COLOR,
                StartButtonPane.ButtonLayout.THREE_BUTTONS,
                FontAwesome.LIST);
        btnLoadAccounts.addActionListener(e -> {
            btnLoadAccounts.setEnabled(false);
            setGuiPath(new LoadAccountsPath(variablesBuilder));
            slider.slideLeft();
        });
        pnlWizardStart.addButton(btnLoadAccounts);

        WizardStartButton btnLoadSettings = new WizardStartButton("Load Settings",
                ACCENT_COLOR,
                StartButtonPane.ButtonLayout.THREE_BUTTONS,
                FontAwesome.UPLOAD);
        btnLoadSettings.addActionListener(e -> {
            btnLoadSettings.setEnabled(false);
            JFileChooser fileChooser = new JFileChooser(Utilities.getFluffeeScriptsDirectory());
            fileChooser.setFileFilter(new FileNameExtensionFilter("Settings", "json"));
            int returnVal = fileChooser.showOpenDialog(pnlWizardStart);
            if (returnVal != JFileChooser.APPROVE_OPTION) {
                btnLoadSettings.setEnabled(true);
                return;
            }
            parseJson(fileChooser.getSelectedFile());
            if (variablesBuilder.getCaptchaSolver() == null) {
                setGuiPath(new LoadAccountsPath(variablesBuilder));
                slider.slideLeft();
            } else {
                setGuiPath(new CreateAccountsPath(variablesBuilder));
                slider.slideLeft();
            }
        });
        pnlWizardStart.addButton(btnLoadSettings);
        slider.addComponent(pnlWizardStart);
    }

    public void parseJson(File jsonFile) {
        try (Reader reader = new FileReader(jsonFile)) {
            Gson gson = new GsonBuilder()
                    .registerTypeAdapter(CaptchaSolver.class, new CaptchaSolver.CaptchaSolverDeserializer())
                    .registerTypeAdapter(ConnectionSettings.class, new ConnectionSettings.ConnectionSettingsDeserializer())
                    .registerTypeAdapter(AccountDetails.class, new AccountDetails.AccountDetailsDeserializer())
                    .create();
            JsonObject jsonMap = gson.fromJson(reader, JsonObject.class);

            if (jsonMap.has("AAG_tutorialConnectionSettings")) {
                 variablesBuilder.setConnectionSettings(gson.fromJson(
                         jsonMap.getAsJsonObject("AAG_tutorialConnectionSettings"), ConnectionSettings.class));
            }
            if (jsonMap.has("AAG_tutorialCaptchaProvider")) {
                variablesBuilder.setCaptchaSolver(gson.fromJson(
                        jsonMap.getAsJsonObject("AAG_tutorialCaptchaProvider"), CaptchaSolver.class));
            }
            if (jsonMap.has("AAG_tutorialAccountsList")) {
                JsonArray jsonAccounts = jsonMap.getAsJsonArray("AAG_tutorialAccountsList");
                AccountDetails[] accountsList = new AccountDetails[jsonAccounts.size()];
                for (int i = 0; i < jsonAccounts.size(); i++) {
                    accountsList[i] = gson.fromJson(jsonAccounts.get(i), AccountDetails.class);
                }
                variablesBuilder.setAccountsList(accountsList);
            }
            AntiBanSingleton.get().setAbcMultiplier(jsonMap.has("AAG_tutorialAbcSleepModifier") ?
                    jsonMap.get("AAG_tutorialAbcSleepModifier").getAsDouble() : 1.0);
            variablesBuilder.setChatSleepModifier(jsonMap.has("AAG_tutorialChatSleepModifier") ?
                    jsonMap.get("AAG_tutorialChatSleepModifier").getAsDouble() : 1.0);
            variablesBuilder.setAccountsEndingLocation(RSLocation.valueOf(
                        jsonMap.has("AAG_tutorialLocation") ?
                        jsonMap.get("AAG_tutorialLocation").getAsString() : RSLocation.NO_WALKING.name()
            ));
            variablesBuilder.setLeaveLastAccountLoggedIn(jsonMap.has("AAG_tutorialStayLoggedIn") ?
                    jsonMap.get("AAG_tutorialStayLoggedIn").getAsBoolean() : false);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static class ScriptSettingsPane extends AbstractWizardStepPanel {

        private FxComboBox<RSLocation> locations;
        private FxCheckBox chkLastLoggedIn;
        private FxSlider sldAbcSleep, sldChatSleep;
        private FxButton btnSaveSettings;
        private Variables.VariablesBuilder variablesBuilder;

        public ScriptSettingsPane(String stepTitle, Variables.VariablesBuilder variablesBuilder) {
            super(stepTitle);
            this.variablesBuilder = variablesBuilder;
        }

        @Override
        protected void setupPane() {
            ColumnPanel contentsPanel = new ColumnPanel();
            InputColumn inputColumn = new InputColumn();
            contentsPanel.addColumn(inputColumn);

            locations = new FxComboBox<>();
            for (RSLocation location : RSLocation.values()) {
                locations.addItem(location);
            }
            inputColumn.addInputPanel(new InputPanel("Final Destination", locations));

            chkLastLoggedIn = new FxCheckBox("Leave last account logged in");
            inputColumn.addInputPanel(new InputPanel("", chkLastLoggedIn));

            sldAbcSleep = new FxSlider(ACCENT_COLOR, 0, 100);
            sldAbcSleep.setValue(100);
            inputColumn.addInputPanel(new InputPanel("ABC2 Sleep Percentage", sldAbcSleep));

            sldChatSleep = new FxSlider(ACCENT_COLOR, 0, 100);
            sldChatSleep.setValue(100);
            inputColumn.addInputPanel(new InputPanel("Chat Sleep Percentage", sldChatSleep));

            btnSaveSettings = new FxButton("Save Settings");
            btnSaveSettings.addActionListener(e -> {
                onStepCompletion();
                JFileChooser fileChooser = new JFileChooser(Utilities.getFluffeeScriptsDirectory());
                fileChooser.setFileFilter(new FileNameExtensionFilter("Script Settings", "json"));
                int returnVal = fileChooser.showSaveDialog(this);
                if (returnVal != JFileChooser.APPROVE_OPTION) {
                    return;
                } else if (!fileChooser.getSelectedFile().getName().endsWith(".json")) {
                    fileChooser.setSelectedFile(new File(fileChooser.getSelectedFile().getName() + ".json"));
                }
            });
            inputColumn.addInputPanel(new InputPanel("", btnSaveSettings));

            setPanelContents(contentsPanel);
        }

        @Override
        protected void onStepCompletion() {
            super.onStepCompletion();
            variablesBuilder.setLeaveLastAccountLoggedIn(chkLastLoggedIn.isSelected());
            variablesBuilder.setAccountsEndingLocation((RSLocation) locations.getSelectedItem());
            AntiBanSingleton.get().setAbcMultiplier(((double)sldAbcSleep.getValue())/100);
            variablesBuilder.setChatSleepModifier(((double)sldChatSleep.getValue())/100);
        }

        public void onAnimationInFinished(int currentStepIndex) {
            super.onAnimationInFinished(currentStepIndex);
            locations.setSelectedItem(variablesBuilder.getAccountsEndingLocation());
            chkLastLoggedIn.setSelected(variablesBuilder.isLeaveLastAccountLoggedIn());
            sldAbcSleep.setValue((int) (AntiBanSingleton.get().getAbcMultiplier() * 100));
            sldChatSleep.setValue((int) (variablesBuilder.getChatSleepModifier() * 100));
        }
    }

    public static class StartScriptPane extends AbstractWizardStepPanel {

        private FxList lstAccounts;
        private FxTextField txtEmailAddress, txtUsername, txtPassword;
        private FxButton btnSaveAccount;
        private Variables.VariablesBuilder variablesBuilder;

        public StartScriptPane(String stepTitle, Variables.VariablesBuilder variablesBuilder) {
            super(stepTitle);
            this.variablesBuilder = variablesBuilder;
        }

        @Override
        public void onAnimationInFinished(int currentStepIndex) {
            super.onAnimationInFinished(currentStepIndex);
            if (variablesBuilder.getAccountsList().length > lstAccounts.getListModel().getSize()) {
                for (AccountDetails account : variablesBuilder.getAccountsList()) {
                    lstAccounts.addItem(account);
                }
            }
        }

        @Override
        protected void onRightButtonClick(ActionEvent event) {
            Component parent = (Component) event.getSource();
            while (parent.getParent() != null) {
                parent = parent.getParent();
            }
            if (this.isStepComplete()) {
                onStepCompletion();
                Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(
                        new CloseGuiEvent(parent)
                );
            }
        }

        @Override
        protected String getRightButtonText() {
            return "Start";
        }

        @Override
        protected void setupPane() {
            ColumnPanel contentsPanel = new ColumnPanel();
            ListColumn listColumn = new ListColumn();
            InputColumn inputColumn = new InputColumn();
            contentsPanel.addColumn(listColumn);
            contentsPanel.addColumn(inputColumn);

            lstAccounts = new FxList();
            lstAccounts.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            lstAccounts.addListSelectionListener(e -> {
                AccountDetails account = (AccountDetails) lstAccounts.getSelectedValue();
                txtEmailAddress.setText(account.getEmail());
                txtPassword.setText(account.getPassword());
                txtUsername.setText(account.getUsername());
            });
            listColumn.setList(lstAccounts);

            txtEmailAddress = new FxTextField();
            inputColumn.addInputPanel(new InputPanel("Email Address", txtEmailAddress));

            txtUsername = new FxTextField();
            inputColumn.addInputPanel(new InputPanel("Username", txtUsername));

            txtPassword = new FxTextField();
            inputColumn.addInputPanel(new InputPanel("Password", txtPassword));

            btnSaveAccount = new FxButton("Save Account");
            btnSaveAccount.addActionListener(e -> {
                AccountDetails account = new AccountDetails(txtEmailAddress.getText(), txtPassword.getText(),
                        txtUsername.getText());
                txtEmailAddress.setText("");
                txtUsername.setText("");
                txtPassword.setText("");
                lstAccounts.getListModel().setElementAt(account, lstAccounts.getSelectedIndex());
            });
            inputColumn.addInputPanel(new InputPanel("", btnSaveAccount));

            setPanelContents(contentsPanel);
        }

        @Override
        protected boolean isStepComplete() {
            return true;
        }

        @Override
        protected void onStepCompletion() {
            super.onStepCompletion();
            AccountDetails[] accountsList = new AccountDetails[lstAccounts.getListModel().getSize()];
            for (int i = 0; i < accountsList.length; i++) {
                accountsList[i] = (AccountDetails) lstAccounts.getListModel().getElementAt(i);
            }
            variablesBuilder.setAccountsList(accountsList);
        }
    }

}
