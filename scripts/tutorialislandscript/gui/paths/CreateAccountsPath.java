package scripts.tutorialislandscript.gui.paths;

import scripts.modules.fluffeesapi.data.accounts.AccountDetails;
import scripts.modules.fluffeesapi.data.network.ConnectionSettings;
import scripts.modules.fluffeesapi.scripting.swingcomponents.frames.FxDialog;
import scripts.modules.fluffeesapi.scripting.swingcomponents.gui.wizard.AbstractWizardPath;
import scripts.modules.fluffeesapi.scripting.swingcomponents.gui.wizard.AbstractWizardStepPanel;
import scripts.modules.fluffeesapi.scripting.swingcomponents.inputs.FxComboBox;
import scripts.modules.fluffeesapi.scripting.swingcomponents.inputs.FxSpinner;
import scripts.modules.fluffeesapi.scripting.swingcomponents.inputs.FxTextField;
import scripts.modules.fluffeesapi.scripting.swingcomponents.inputs.listeners.IntegerInputValidator;
import scripts.modules.fluffeesapi.scripting.swingcomponents.panels.InputPanel;
import scripts.modules.fluffeesapi.scripting.swingcomponents.panels.stepcontents.ColumnPanel;
import scripts.modules.fluffeesapi.scripting.swingcomponents.panels.stepcontents.InputColumn;
import scripts.modules.fluffeesapi.web.accountCreation.captcha.AntiCaptcha;
import scripts.modules.fluffeesapi.web.accountCreation.captcha.CaptchaSolver;
import scripts.modules.fluffeesapi.web.accountCreation.captcha.TwoCaptcha;
import scripts.tutorialislandscript.data.Variables;
import scripts.tutorialislandscript.gui.TutorialIslandGui;

import javax.swing.*;
import java.awt.*;

public class CreateAccountsPath extends AbstractWizardPath {

    private String dialogErrorMessage = "We cannot continue to the next step, as there were the following errors in " +
            "your submission. For further help, please click the ? in the top left corner of the GUI. <ul>";
    private Variables.VariablesBuilder variablesBuilder;

    public CreateAccountsPath(Variables.VariablesBuilder variablesBuilder) {
        super();
        this.variablesBuilder = variablesBuilder;
    }

    @Override
    protected int getNumberSteps() {
        return 4;
    }

    @Override
    protected String[] getStepNames() {
        return new String[]{"Account Creation", "Proxy Settings", "Script Settings", "Review Accounts"};
    }

    @Override
    protected Color getStepButtonBackground() {
        return TutorialIslandGui.ACCENT_COLOR;
    }

    @Override
    protected AbstractWizardStepPanel[] getSteps() {
        return new AbstractWizardStepPanel[]{
            new CreateAccountsStepPanel("Account Creation", variablesBuilder),
            new ProxySettingsPane("Proxy Settings", variablesBuilder),
            new TutorialIslandGui.ScriptSettingsPane("Script Settings", variablesBuilder),
            new TutorialIslandGui.StartScriptPane("Start Script", variablesBuilder),
        };
    }


    public class CreateAccountsStepPanel extends AbstractWizardStepPanel {

        private FxTextField txtEmailAddress, txtAccountUsername, txtAccountPassword, txtCaptchaKey;
        private FxSpinner spnNumberAccounts;
        private FxComboBox<String> cmbCaptchaProvider;
        private Variables.VariablesBuilder variablesBuilder;

        public CreateAccountsStepPanel(String title, Variables.VariablesBuilder variablesBuilder) {
            super(title); //"Account Creation"
            this.variablesBuilder = variablesBuilder;
        }

        @Override
        protected boolean getLeftButtonVisible() {
            return false;
        }

        @Override
        protected void setupPane() {
            ColumnPanel contentsPanel = new ColumnPanel();
            InputColumn inputColumn = new InputColumn();
            contentsPanel.addColumn(inputColumn);

            txtEmailAddress = new FxTextField();
            inputColumn.addInputPanel(new InputPanel("Email Address", txtEmailAddress));

            txtAccountUsername = new FxTextField();
            inputColumn.addInputPanel(new InputPanel("Account Username", txtAccountUsername));

            txtAccountPassword = new FxTextField();
            inputColumn.addInputPanel(new InputPanel("Account Password", txtAccountPassword));

            spnNumberAccounts = new FxSpinner(1, 1, 10000, 1);
            ((JSpinner.DefaultEditor)spnNumberAccounts.getEditor()).getTextField().addKeyListener(new IntegerInputValidator());
            inputColumn.addInputPanel(new InputPanel("Number of Accounts", spnNumberAccounts));

            cmbCaptchaProvider = new FxComboBox<>();
            cmbCaptchaProvider.addItem(TwoCaptcha.getPrettyName());
            cmbCaptchaProvider.addItem(AntiCaptcha.getPrettyName());
            inputColumn.addInputPanel(new InputPanel("Captcha Provider", cmbCaptchaProvider));

            txtCaptchaKey = new FxTextField();
            inputColumn.addInputPanel(new InputPanel("Captcha Key", txtCaptchaKey));

            this.setPanelContents(contentsPanel);
        }

        @Override
        protected boolean isStepComplete() {
            String submissionErrors = "";
            if (!(spnNumberAccounts.getValue() instanceof Integer)) {
                submissionErrors += "<li>The value in the number of accounts field is not an integer.</li>";
            } else if ((Integer) spnNumberAccounts.getValue() > 1 &&
                    (!txtAccountUsername.getText().contains("[") || !txtEmailAddress.getText().contains("["))) {
                submissionErrors += "<li>You are trying to create more than 1 accounts, but either your email address or" +
                        "the account username is not using some sort of generated value.</li>";
            }

            if (txtCaptchaKey.getText().isEmpty()) {
                submissionErrors += "<li>Your captcha key is empty, you need a captcha key to make accounts.</li>";
            }
            if (!txtEmailAddress.getText().contains("@") || !txtEmailAddress.getText().contains(".")) {
                submissionErrors += "<li>Your email address appears to be invalid. Please double check to ensure it" +
                        " has an @ symbol and a .</li>";
            }
            //TODO: Implement check to ensure creation is valid
            if (!submissionErrors.isEmpty()) {
                submissionErrors = submissionErrors + "</ul>";
                FxDialog.errorDialogFactory(null, "Error", dialogErrorMessage + submissionErrors);
                return false;
            }
            return true;
        }

        @Override
        protected void onStepCompletion() {
            super.onStepCompletion();

            variablesBuilder.setAccountsList(AccountDetails.factoryGenerateMultipleAccounts(
                    txtEmailAddress.getText(), txtAccountPassword.getText(), txtAccountUsername.getText(),
                    (Integer) spnNumberAccounts.getValue()
            ));
            variablesBuilder.setCaptchaSolver(
                    CaptchaSolver.getCaptchaSolver(
                        (String) cmbCaptchaProvider.getSelectedItem(),
                        txtCaptchaKey.getText()
                    )
            );
        }

        @Override
        public void onAnimationInFinished(int currentStepIndex) {
            super.onAnimationInFinished(currentStepIndex);

            if (variablesBuilder.getAccountsList().length < 1) {
                return;
            }
            txtEmailAddress.setText(variablesBuilder.getAccountsList()[0].getEmail());
            txtAccountUsername.setText(variablesBuilder.getAccountsList()[0].getUsername());
            txtAccountPassword.setText(variablesBuilder.getAccountsList()[0].getPassword());
            spnNumberAccounts.setValue(variablesBuilder.getAccountsList().length);
            cmbCaptchaProvider.setSelectedIndex(variablesBuilder.getCaptchaSolver() instanceof TwoCaptcha ? 0 : 1);
            txtCaptchaKey.setText(variablesBuilder.getCaptchaSolver().getCaptchaKey());
        }
    }

    private class ProxySettingsPane extends AbstractWizardStepPanel {

        private FxTextField txtProxyIp, txtProxyPort, txtProxyUsername, txtProxyPassword;
        private Variables.VariablesBuilder variablesBuilder;

        public ProxySettingsPane(String title, Variables.VariablesBuilder variablesBuilder) {
            super(title);
            this.variablesBuilder = variablesBuilder;
        }

        @Override
        protected void setupPane() {
            ColumnPanel contentsPanel = new ColumnPanel();
            InputColumn inputColumn = new InputColumn();
            contentsPanel.addColumn(inputColumn);

            txtProxyIp = new FxTextField();
            inputColumn.addInputPanel(new InputPanel("IP Address", txtProxyIp));

            txtProxyPort = new FxTextField();
            txtProxyPort.addKeyListener(new IntegerInputValidator());
            inputColumn.addInputPanel(new InputPanel("Port", txtProxyPort));

            txtProxyUsername = new FxTextField();
            inputColumn.addInputPanel(new InputPanel("Proxy Username", txtProxyUsername));

            txtProxyPassword = new FxTextField();
            inputColumn.addInputPanel(new InputPanel("Proxy Username", txtProxyPassword));

            this.setPanelContents(contentsPanel);
        }

        @Override
        protected boolean isStepComplete() {
            String submissionErrors = "";
            if (txtProxyIp.getText().isEmpty()) {
                return true;
            } else if (txtProxyPort.getText().isEmpty() || !isInteger(txtProxyPort.getText())) {
                submissionErrors += "<li>The proxy port is either empty or not a number. You cannot use a proxy " +
                        "without the proper port.</li>";
            } else if (txtProxyPassword.getText().isEmpty() ^ txtProxyUsername.getText().isEmpty()) {
                submissionErrors += "<li>You're attempting to use a user:pass authenticated proxy without" +
                        "entering both a username and a password.";
            }
            if (!submissionErrors.isEmpty()) {
                submissionErrors = submissionErrors + "</ul>";
                FxDialog.errorDialogFactory(null, "Error", dialogErrorMessage + submissionErrors);
                return false;
            }
            return true;
        }

        @Override
        protected void onStepCompletion() {
            super.onStepCompletion();
            if (txtProxyIp.getText().isEmpty()) {
                variablesBuilder.setConnectionSettings(
                        new ConnectionSettings(null)
                );
            } else {
                variablesBuilder.setConnectionSettings(
                        new ConnectionSettings(
                                txtProxyIp.getText(),
                                Integer.parseInt(txtProxyPort.getText()),
                                txtProxyUsername.getText(),
                                txtProxyPassword.getText()
                        )
                );
            }
        }

        public void onAnimationInFinished(int currentStepIndex) {
            super.onAnimationInFinished(currentStepIndex);
            
            if (variablesBuilder.getConnectionSettings() == null ||
                    variablesBuilder.getConnectionSettings().getProxy() == null) {
                return;
            }
            txtProxyIp.setText(variablesBuilder.getConnectionSettings().getIpAddress());
            txtProxyPort.setText(Integer.toString(variablesBuilder.getConnectionSettings().getProxyPort()));
            txtProxyUsername.setText(variablesBuilder.getConnectionSettings().getProxyUsername());
            txtProxyPassword.setText(variablesBuilder.getConnectionSettings().getProxyPassword());
        }
    }

    private static boolean isInteger(String str) {
        if (str == null || str.length() == 0) {
            return false;
        }
        int length = str.length();
        int i = str.charAt(0) == '-' ? 1 : 0;
        for (; i < length; i++) {
            char c = str.charAt(i);
            if (c < '0' || c > '9') {
                return false;
            }
        }
        return length > 1;
    }
}
