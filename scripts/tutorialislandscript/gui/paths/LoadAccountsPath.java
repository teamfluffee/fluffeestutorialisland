package scripts.tutorialislandscript.gui.paths;

import scripts.modules.fluffeesapi.data.accounts.AccountDetails;
import scripts.modules.fluffeesapi.scripting.swingcomponents.gui.wizard.AbstractWizardPath;
import scripts.modules.fluffeesapi.scripting.swingcomponents.gui.wizard.AbstractWizardStepPanel;
import scripts.modules.fluffeesapi.scripting.swingcomponents.inputs.FxList;
import scripts.modules.fluffeesapi.scripting.swingcomponents.inputs.FxTextField;
import scripts.modules.fluffeesapi.scripting.swingcomponents.inputs.buttons.FxButton;
import scripts.modules.fluffeesapi.scripting.swingcomponents.panels.InputPanel;
import scripts.modules.fluffeesapi.scripting.swingcomponents.panels.stepcontents.ColumnPanel;
import scripts.modules.fluffeesapi.scripting.swingcomponents.panels.stepcontents.InputColumn;
import scripts.modules.fluffeesapi.scripting.swingcomponents.panels.stepcontents.ListColumn;
import scripts.modules.fluffeesapi.utilities.Utilities;
import scripts.tutorialislandscript.data.Variables;
import scripts.tutorialislandscript.gui.TutorialIslandGui;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class LoadAccountsPath extends AbstractWizardPath {

    private Variables.VariablesBuilder variablesBuilder;

    public LoadAccountsPath(Variables.VariablesBuilder variablesBuilder) {
        super();
        this.variablesBuilder = variablesBuilder;
    }

    @Override
    protected int getNumberSteps() {
        return 3;
    }

    @Override
    protected String[] getStepNames() {
        return new String[]{"Load Accounts", "Script Settings", "Review Accounts"};
    }

    @Override
    protected Color getStepButtonBackground() {
        return TutorialIslandGui.ACCENT_COLOR;
    }

    @Override
    protected AbstractWizardStepPanel[] getSteps() {
        return new AbstractWizardStepPanel[]{
                new LoadAccountsStepPanel("Load Accounts", variablesBuilder),
                new TutorialIslandGui.ScriptSettingsPane("Script Settings", variablesBuilder),
                new TutorialIslandGui.StartScriptPane("Start Script", variablesBuilder),
        };
    }

    private class LoadAccountsStepPanel extends AbstractWizardStepPanel {

        private FxList lstAccounts;
        private FxTextField txtEmailAddress, txtUsername, txtPassword;
        private FxButton btnSaveAccount, btnLoadAccounts;
        private ArrayList<AccountDetails> accounts;
        private Variables.VariablesBuilder variablesBuilder;

        public LoadAccountsStepPanel(String title, Variables.VariablesBuilder variablesBuilder) {
            super(title);
            this.variablesBuilder = variablesBuilder;
        }

        @Override
        protected boolean getLeftButtonVisible() {
            return false;
        }

        @Override
        protected void setupPane() {
            accounts = new ArrayList<>();
            ColumnPanel contentsPanel = new ColumnPanel();
            ListColumn listColumn = new ListColumn();
            InputColumn inputColumn = new InputColumn();
            contentsPanel.addColumn(listColumn);
            contentsPanel.addColumn(inputColumn);

            lstAccounts = new FxList();
            listColumn.setList(lstAccounts);

            txtEmailAddress = new FxTextField();
            inputColumn.addInputPanel(new InputPanel("Email Address", txtEmailAddress));

            txtUsername = new FxTextField();
            inputColumn.addInputPanel(new InputPanel("Username", txtUsername));

            txtPassword = new FxTextField();
            inputColumn.addInputPanel(new InputPanel("Password", txtPassword));

            btnSaveAccount = new FxButton("Save Account");
            btnSaveAccount.addActionListener(e -> {
                accounts.add(new AccountDetails(txtEmailAddress.getText(), txtPassword.getText(),
                        txtUsername.getText(), true));
                lstAccounts.addItem(accounts.get(accounts.size()-1));
            });
            inputColumn.addInputPanel(new InputPanel("", btnSaveAccount));

            btnLoadAccounts = new FxButton("Load Accounts");
            inputColumn.addInputPanel(new InputPanel("", btnLoadAccounts));
            btnLoadAccounts.addActionListener(e -> {
                JFileChooser fileChooser = new JFileChooser(Utilities.getFluffeeScriptsDirectory());
                fileChooser.setFileFilter(new FileNameExtensionFilter("Email:User:Pass Text Files", "txt"));
                int returnVal = fileChooser.showOpenDialog(this);
                if (returnVal != JFileChooser.APPROVE_OPTION) {
                    return;
                }
                accounts = loadAccounts(fileChooser.getSelectedFile());
            });

            this.setPanelContents(contentsPanel);
        }

        @Override
        protected void onStepCompletion() {
            super.onStepCompletion();
            variablesBuilder.setAccountsList(accounts.toArray(new AccountDetails[0]));
        }

        @Override
        protected boolean isStepComplete() {
            return super.isStepComplete();
        }

        private ArrayList<AccountDetails> loadAccounts(File accountsFile) {
            ArrayList<AccountDetails> accountDetails = new ArrayList<>();
            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(accountsFile))) {
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    String[] lineParts = line.split(":");
                    if (lineParts.length != 3) {
                        continue;
                    }
                    accountDetails.add(new AccountDetails(lineParts[0], lineParts[2], lineParts[1], true));
                    lstAccounts.addItem(accountDetails.get(accountDetails.size()-1));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return accountDetails;
        }

        @Override
        public void onAnimationInFinished(int currentPanelIndex) {
            super.onAnimationInFinished(currentPanelIndex);
            accounts = new ArrayList<>(Arrays.asList(variablesBuilder.getAccountsList()));
        }
    }
}
