package scripts.tutorialislandscript.missions;

import org.tribot.api2007.Player;
import org.tribot.api2007.WebWalking;
import scripts.modules.fluffeesapi.scripting.frameworks.mission.missiontypes.taskmissions.TaskMission;
import scripts.modules.fluffeesapi.scripting.frameworks.task.Priority;
import scripts.modules.fluffeesapi.scripting.frameworks.task.tasktypes.SuccessTask;
import scripts.modules.fluffeesapi.scripting.frameworks.task.TaskSet;
import scripts.tutorialislandscript.data.RSLocation;

public class WalkingMission implements TaskMission {

    private TaskSet taskSet;
    private RSLocation targetLocation;

    public WalkingMission(RSLocation targetLocation) {
        this.targetLocation = targetLocation;

        taskSet = new TaskSet();
        taskSet.add(new WalkingTask());
    }

    @Override
    public TaskSet getTaskSet() {
        return taskSet;
    }

    @Override
    public String getMissionName() {
        return "Walking Mission";
    }

    @Override
    public boolean isMissionValid() {
        return targetLocation != RSLocation.NO_WALKING;
    }

    @Override
    public boolean isMissionCompleted() {
        return targetLocation.getTargetTile().distanceTo(Player.getPosition()) < 7;
    }

    private class WalkingTask extends SuccessTask {

        @Override
        public Priority priority() {
            return Priority.MEDIUM;
        }

        @Override
        public boolean isValid() {
            return targetLocation.getTargetTile().distanceTo(Player.getPosition()) > 7;
        }

        @Override
        public void successExecute() {
            WebWalking.walkTo(targetLocation.getTargetTile());
        }

        @Override
        public String getStatus() {
            return "Walking to end tile";
        }
    }
}
