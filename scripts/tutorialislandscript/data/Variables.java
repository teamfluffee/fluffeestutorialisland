package scripts.tutorialislandscript.data;

import scripts.modules.fluffeesapi.data.accounts.AccountDetails;
import scripts.modules.fluffeesapi.data.network.ConnectionSettings;
import scripts.modules.fluffeesapi.data.structures.PrivateSingleton;
import scripts.modules.fluffeesapi.web.accountCreation.captcha.CaptchaSolver;

public class Variables extends PrivateSingleton {

    private int currentAccountIndex;
    private boolean leaveLastAccountLoggedIn, switchAccounts;
    private RSLocation accountsEndingLocation;
    private AccountDetails[] accountsList;
    private CaptchaSolver captchaSolver;
    private ConnectionSettings connectionSettings;
    private double chatSleepModifier;

    private Variables(Class callingClass,
                      boolean leaveLastAccountLoggedIn,
                      RSLocation accountsEndingLocation,
                      AccountDetails[] accountsList,
                      CaptchaSolver captchaSolver,
                      ConnectionSettings connectionSettings,
                      double chatSleepModifier) throws IllegalAccessException {
        super(callingClass);
        this.leaveLastAccountLoggedIn = leaveLastAccountLoggedIn;
        this.accountsEndingLocation = accountsEndingLocation;
        this.accountsList = accountsList;
        this.captchaSolver = captchaSolver;
        this.connectionSettings = connectionSettings;
        this.chatSleepModifier = chatSleepModifier;
        this.currentAccountIndex = 0;
    }

    public int getCurrentAccountIndex() {
        return currentAccountIndex;
    }

    public int getTotalAccountsNumber() {
        return getAccountsList().length;
    }

    public boolean isLeaveLastAccountLoggedIn() {
        return leaveLastAccountLoggedIn;
    }

    public boolean isSwitchAccounts() {
        return switchAccounts;
    }

    public RSLocation getAccountsEndingLocation() {
        return accountsEndingLocation;
    }

    public AccountDetails[] getAccountsList() {
        return accountsList;
    }

    public AccountDetails getCurrentAccount() {
        return getAccountsList()[getCurrentAccountIndex()];
    }

    public boolean hasNextAccount() {
        return getTotalAccountsNumber() > getCurrentAccountIndex();
    }

    public void moveToNextAccount() {
        this.currentAccountIndex++;
    }

    public CaptchaSolver getCaptchaSolver() {
        return captchaSolver;
    }

    public ConnectionSettings getConnectionSettings() {
        return connectionSettings;
    }

    public double getChatSleepModifier() {
        return chatSleepModifier;
    }

    @Override
    public Class getAllowedClass() {
        return scripts.tutorialislandscript.Main.class;
    }

    public static class VariablesBuilder {
        private boolean leaveLastAccountLoggedIn;
        private RSLocation accountsEndingLocation;
        private AccountDetails[] accountsList;
        private CaptchaSolver captchaSolver;
        private ConnectionSettings connectionSettings;
        private double chatSleepModifier;
        private Class callingClass;

        public VariablesBuilder(Class callingClass) {
            this.callingClass = callingClass;
            this.leaveLastAccountLoggedIn = false;
            this.accountsEndingLocation = RSLocation.NO_WALKING;
            this.accountsList = new AccountDetails[0];
            this.chatSleepModifier = 1.0;
        }

        public VariablesBuilder setLeaveLastAccountLoggedIn(boolean leaveLastAccountLoggedIn) {
            this.leaveLastAccountLoggedIn = leaveLastAccountLoggedIn;
            return this;
        }

        public VariablesBuilder setAccountsEndingLocation(RSLocation endingLocation) {
            this.accountsEndingLocation = endingLocation;
            return this;
        }

        public VariablesBuilder setAccountsList(AccountDetails[] accountsList) {
            this.accountsList = accountsList;
            return this;
        }

        public VariablesBuilder setCaptchaSolver(CaptchaSolver captchaSolver) {
            this.captchaSolver = captchaSolver;
            return this;
        }

        public VariablesBuilder setConnectionSettings(ConnectionSettings connectionSettings) {
            this.connectionSettings = connectionSettings;
            return this;
        }

        public VariablesBuilder setChatSleepModifier(double chatSleepModifier) {
            this.chatSleepModifier = chatSleepModifier;
            return this;
        }

        public boolean isLeaveLastAccountLoggedIn() {
            return leaveLastAccountLoggedIn;
        }

        public RSLocation getAccountsEndingLocation() {
            return accountsEndingLocation;
        }

        public AccountDetails[] getAccountsList() {
            return accountsList;
        }

        public CaptchaSolver getCaptchaSolver() {
            return captchaSolver;
        }

        public ConnectionSettings getConnectionSettings() {
            return connectionSettings;
        }

        public double getChatSleepModifier() {
            return chatSleepModifier;
        }

        public Variables build() throws IllegalAccessException {
            return new Variables(callingClass,
                    leaveLastAccountLoggedIn,
                    accountsEndingLocation,
                    accountsList,
                    captchaSolver,
                    connectionSettings,
                    chatSleepModifier);
        }
    }
}
