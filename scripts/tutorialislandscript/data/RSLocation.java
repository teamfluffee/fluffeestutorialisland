package scripts.tutorialislandscript.data;

import org.tribot.api2007.types.RSTile;

public enum RSLocation {

    NO_WALKING(null, "Don't Walk Anywhere"),
    GRAND_EXCHANGE (new RSTile(3164, 3486, 0), "Grand Exchange"),
    VARROCK_WEST_BANK (new RSTile(3183,3440,0), "Varrock West Bank"),
    VARROCK_SQUARE (new RSTile(3212, 3428, 0), "Varrock Square"),
    VARROCK_EAST_BANK (new RSTile(3253, 3420, 0), "Varrock East Bank"),
    LUMBRIDGE_BANK(new RSTile(3209, 3218, 2), "Lumbridge Bank"),
    AL_KHARID_BANK(new RSTile(3269, 3166, 0), "Al-Kharid Bank"),
    DRAYNOR_BANK(new RSTile(3092, 3243, 0), "Draynor Bank"),
    PORT_SARIM(new RSTile(3027, 3224, 0), "Port Sarim"),
    RIMMINGTON(new RSTile(2956, 3212, 0), "Rimmington"),
    FALADOR_EAST_BANK(new RSTile(3012, 3356, 0), "Falador East Bank"),
    FALADOR_WEST_BANK(new RSTile(2945, 3369, 0), "Falador West Bank"),
    EDGEVILLE_BANK(new RSTile(3093, 3496, 0), "Edgeville Bank");

    private RSTile targetTile;
    private String prettyName;

    RSLocation(RSTile targetTile, String prettyName) {
        this.targetTile = targetTile;
        this.prettyName = prettyName;
    }

    public RSTile getTargetTile() {
        return targetTile;
    }

    public String getPrettyName() {
        return prettyName;
    }

    public String toString() {
        return this.prettyName;
    }

}
