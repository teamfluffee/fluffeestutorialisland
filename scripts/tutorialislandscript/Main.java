package scripts.tutorialislandscript;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.tribot.api.General;
import org.tribot.api2007.Login;
import org.tribot.script.ScriptManifest;
import org.tribot.script.interfaces.Arguments;
import org.tribot.script.interfaces.Painting;
import org.tribot.script.interfaces.Starting;
import scripts.modules.accountcreator.AccountCreator;
import scripts.fluffeesLogin.FluffeesLogin;
import scripts.modules.fluffeesapi.data.accounts.AccountDetails;
import scripts.modules.fluffeesapi.data.network.ConnectionSettings;
import scripts.modules.fluffeesapi.scripting.antiban.AntiBanSingleton;
import scripts.modules.fluffeesapi.scripting.frameworks.mission.missiontypes.Mission;
import scripts.modules.fluffeesapi.scripting.frameworks.mission.missiontypes.TaskFinish;
import scripts.modules.fluffeesapi.scripting.frameworks.mission.scriptTypes.MissionManagerScript;
import scripts.modules.fluffeesapi.scripting.painting.scriptPaint.ScriptPaint;
import scripts.modules.fluffeesapi.utilities.ArgumentUtilities;
import scripts.modules.fluffeesapi.utilities.FileUtilities;
import scripts.modules.fluffeesapi.utilities.Utilities;
import scripts.modules.fluffeesapi.web.accountCreation.AccountCreation;
import scripts.modules.fluffeesapi.web.accountCreation.captcha.CaptchaSolver;
import scripts.logout.Logout;
import scripts.modules.tutorialisland.TutorialIsland;
import scripts.modules.tutorialisland.missions.charactersetup.StyleMission;
import scripts.tutorialislandscript.data.RSLocation;
import scripts.tutorialislandscript.data.Variables;
import scripts.tutorialislandscript.gui.TutorialIslandGui;
import scripts.tutorialislandscript.missions.WalkingMission;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.*;

@ScriptManifest(
    authors = "Fluffee",
    category = "Quests",
    name = "Tutorial Island",
    description = "Local version.",
    version = 4.03,
    gameMode = 1
)

public class Main extends MissionManagerScript implements Painting, Starting, Arguments {

    private LinkedList<Mission> missionList = new LinkedList<>();
    private Variables variables = null;
    private Variables.VariablesBuilder variablesBuilder;
    private String emailGenerationString, usernameGenerationString, passwordGenerationString;
    private int accountGenerationOffset = 0;

    @Override
    public void onStart() {
        super.onStart();
        this.setLoginBotState(false);
    }

    @Override
    public void onGuiClosed() {
        super.onGuiClosed();
        if (variables == null) {
            try {
                variables = variablesBuilder.build();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public LinkedList<Mission> getMissions() {
        return missionList;
    }

    @Override
    public void initializeMissionList() {
        emailGenerationString = variables.getCurrentAccount().getEmail();
        usernameGenerationString = variables.getCurrentAccount().getUsername();
        passwordGenerationString = variables.getCurrentAccount().getPassword();
        variables.getCurrentAccount().setEmail(generateValue(emailGenerationString));
        variables.getCurrentAccount().setPassword(generateValue(passwordGenerationString));
        variables.getCurrentAccount().setUsername(generateValue(usernameGenerationString));

        if (!variables.getCurrentAccount().isAccountCreated()) {
            missionList.add(new AccountCreator(variables.getCurrentAccount(), variables.getConnectionSettings(),
                    variables.getCaptchaSolver()));
        }
        missionList.add(new FluffeesLogin(variables.getCurrentAccount()));
        missionList.add(
                new TutorialIsland(
                        variables.getChatSleepModifier(),
                        variables.getCurrentAccount().getUsername()
                )
        );
        missionList.add(
                new WalkingMission(
                        variables.getAccountsEndingLocation()
                )
        );

        if (variables.hasNextAccount() || !variables.isLeaveLastAccountLoggedIn()) {
            missionList.add(new Logout());
        }
    }

    @Override
    public boolean shouldStopExecution(TaskFinish taskFinish) {
        if (getCurrentMission() instanceof AccountCreator) {
            return handleAccountCreationFinishes((AccountCreator.MissionFinishes) taskFinish);
        } else if (getCurrentMission() instanceof TutorialIsland) {
            return handleTutorialIslandFinishes(taskFinish);
        } else {
            return super.shouldStopExecution(taskFinish);
        }
    }

    @Override
    public boolean shouldLoopMissions() {
        return true;
    }

    @Override
    public boolean isMissionValid() {
        return variables.hasNextAccount();
    }

    @Override
    public boolean isMissionCompleted() {
        return !variables.hasNextAccount()
                && (variables.isLeaveLastAccountLoggedIn() || Login.getLoginState() != Login.STATE.INGAME);
    }

    @Override
    public ArrayList<AbstractMap.SimpleEntry<String, String>> getPaintFields() {
        int currentIndex = variables == null ? 0 : (variables.getCurrentAccountIndex() + 1);
        int totalAccountNumber = variables == null ? 0 : variables.getTotalAccountsNumber();

        ArrayList<AbstractMap.SimpleEntry<String, String>> fields = super.getPaintFields();
        fields.add(new AbstractMap.SimpleEntry<>("Account", currentIndex + " of " + totalAccountNumber));
        return fields;
    }

    @Override
    public void preScriptTasks() {
        this.setScriptPaint(
            new ScriptPaint.Builder(ScriptPaint.hex2Rgb("#b7c0ee"), "Tutorial Island")
                .addField("Version", Double.toString(getClass().getAnnotation(ScriptManifest.class).version()
                )
            ).build()
        );

        variablesBuilder = new Variables.VariablesBuilder(this.getClass());

        this.setGui(new TutorialIslandGui(variablesBuilder));
    }

    @Override
    public MissionGameState getMissionGameState() {
        return MissionGameState.BOTH;
    }

    @Override
    public void passArguments(HashMap<String, String> incomingArguments) {
        HashMap<String, String> parsedArguments = ArgumentUtilities.get(incomingArguments);
        if (parsedArguments.containsKey("settingsFile")) {
            File settingsFile = new File(Utilities.getFluffeeScriptsDirectory() + parsedArguments.get("settingsFile"));
            if (settingsFile.exists() && !settingsFile.isDirectory()) {
                this.hasArguments = true;
                parseJson(settingsFile);
            }
        }
    }

    private void parseJson(File jsonFile) {
        try (Reader reader = new FileReader(jsonFile)) {
            Gson gson = new GsonBuilder()
                    .registerTypeAdapter(CaptchaSolver.class, new CaptchaSolver.CaptchaSolverDeserializer())
                    .registerTypeAdapter(ConnectionSettings.class, new ConnectionSettings.ConnectionSettingsDeserializer())
                    .registerTypeAdapter(AccountDetails.class, new AccountDetails.AccountDetailsDeserializer())
                    .create();
            JsonObject jsonMap = gson.fromJson(reader, JsonObject.class);

            Variables.VariablesBuilder builder = new Variables.VariablesBuilder(this.getClass());
            if (jsonMap.has("AAG_tutorialConnectionSettings")) {
                builder.setConnectionSettings(gson.fromJson(jsonMap.getAsJsonObject("AAG_tutorialConnectionSettings"),
                        ConnectionSettings.class));
            }
            if (jsonMap.has("AAG_tutorialCaptchaProvider")) {
                builder.setCaptchaSolver(
                        gson.fromJson(jsonMap.getAsJsonObject("AAG_tutorialCaptchaProvider"), CaptchaSolver.class));
            }
            if (jsonMap.has("AAG_tutorialAccountsList")) {
                JsonArray jsonAccounts = jsonMap.getAsJsonArray("AAG_tutorialAccountsList");
                AccountDetails[] accounts = new AccountDetails[jsonAccounts.size()];
                for (int i = 0; i < jsonAccounts.size(); i++) {
                    accounts[i] = gson.fromJson(jsonAccounts.get(i), AccountDetails.class);
                }
                builder.setAccountsList(accounts);
            }
            AntiBanSingleton.get()
                    .setAbcMultiplier(jsonMap.has("AAG_tutorialAbcSleepModifier")
                            ? jsonMap.get("AAG_tutorialAbcSleepModifier").getAsDouble()
                            : 1.0);
            builder.setChatSleepModifier(jsonMap.has("AAG_tutorialChatSleepModifier")
                    ? jsonMap.get("AAG_tutorialChatSleepModifier").getAsDouble()
                    : 1.0);
            builder.setAccountsEndingLocation(RSLocation
                    .valueOf(jsonMap.has("AAG_tutorialLocation") ? jsonMap.get("AAG_tutorialLocation").getAsString()
                            : RSLocation.NO_WALKING.name()));
            builder.setLeaveLastAccountLoggedIn(
                    jsonMap.has("AAG_tutorialStayLoggedIn") &&
                            jsonMap.get("AAG_tutorialStayLoggedIn").getAsBoolean()
            );
            variables = builder.build();
        } catch (IOException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private String generateValue(String generationString) {
        ArrayList<String> generationSplit = AccountCreation.stringSplitter(generationString);
        generationSplit = AccountCreation.processStringList(generationSplit, accountGenerationOffset);
        return AccountCreation.arrayListToString(generationSplit);
    }

    private boolean handleAccountCreationFinishes(AccountCreator.MissionFinishes taskFinish) {
        switch (taskFinish) {
        case EMAIL_ADDRESS_TAKEN:
            accountGenerationOffset += 1;
            variables.getCurrentAccount().setEmail(generateValue(emailGenerationString));
            return false;
        case SCAM_PASSWORD:
            if (passwordGenerationString.contains("[")) {
                accountGenerationOffset += 1;
                variables.getCurrentAccount().setPassword(generateValue(passwordGenerationString));
                return false;
            } else {
                return true;
            }
        case SKIPPING_ACCOUNT:
            variables.moveToNextAccount();
            return false;
        case ACCOUNT_CREATED:
            variables.getCurrentAccount().setUsername(generateValue(usernameGenerationString));
            ListIterator<Mission> missionListIterator = missionList.listIterator();
            while (missionListIterator.hasNext()) {
                Mission currentMission = missionListIterator.next();
                if (currentMission instanceof TutorialIsland) {
                    currentMission.postMission();
                    missionListIterator.remove();
                    missionListIterator.add(new TutorialIsland(variables.getChatSleepModifier(),
                            variables.getCurrentAccount().getUsername()));
                }
                General.sleep(300);
            }
            return false;
        default:
            return super.shouldStopExecution(taskFinish);
        }
    }

    private boolean handleTutorialIslandFinishes(TaskFinish taskFinish) {
        if (taskFinish.getFinishType() == TaskFinish.FinishTypes.SUCCESS) {
            String textToWrite = variables.getCurrentAccount().getEmail() + ":"
                    + variables.getCurrentAccount().getPassword() + System.getProperty("line.separator");
            String fileName = File.separator + "FluffeeScripts" + File.separator + "TutorialIsland" + File.separator
                    + General.getTRiBotUsername() + "_Tutorial_Accounts.txt";
            if (!FileUtilities.checkExistance(fileName)) {
                FileUtilities.createFile(textToWrite, fileName);
            } else {
                FileUtilities.appendToFile(fileName, textToWrite);
            }
            variables.moveToNextAccount();
            return false;
        } else if (taskFinish == StyleMission.MissionFinishes.NAME_UNAVAILABLE) {
            accountGenerationOffset += 1;
            variables.getCurrentAccount().setUsername(generateValue(usernameGenerationString));

            missionList.addFirst(
                    new TutorialIsland(variables.getChatSleepModifier(), variables.getCurrentAccount().getUsername()));
            return false;
        } else {
            return super.shouldStopExecution(taskFinish);
        }
    }

}
